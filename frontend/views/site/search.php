<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Search';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This is the Search page. Here will be the search functionality for the website.</p>
</div>